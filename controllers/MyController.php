<?php

namespace app\controllers;

use yii\web\Controller;

class MyController extends Controller
{

    public function behaviors()
    {
        return [
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($fio = 'Укажите Fio')
    {
        return $fio;
    }
    public function actionAbout()
    {
        return $this->render('about', [
            'hello' => 'Chto-to'
        ]);
    }

    public function actionStyle()
    {
        return $this->render('style', [
            'nazvanie' => 'Chto-to'
        ]);
    }

    public function actionCringe()
    {
        return $this->render('cringe', [
            'skill' => "Rage"
        ]);
    }


}


